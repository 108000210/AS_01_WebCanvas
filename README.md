# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 

    我的canvas有幾個基本功能:
        * 畫筆（Brush）
        * 橡皮擦（Eraser） 
        * 文字輸入框（Text）
        * 圓形繪製（Circle）
        * 三角形繪製（Triangle）
        * 方形繪製（Rectangle）
        * Undo, Redo 
        * 清空（Clear）
        * 選擇字型大小、字體
        * 選擇顏色
        * 線條粗細
        * 上傳、下載檔案

        文字輸入框在確定要輸入後，需按下enter，才能讓輸入文字顯示在canvas上；而其他繪圖功能皆是在canvas上按下滑鼠拖移。
        選擇顏色會影響到所有繪圖功能；而線條粗細會影響到畫筆及各種形狀繪製。
![img](./canvas_img.png)
  

### Function description

        我的code的架構基本上是由偵測canvas上的三個事件組成：mousedown, mousemove, mouseup. mousedown的時候紀錄游標初始位置，move時開始依照當前的模式繪畫，up時完成繪圖。
        我有用一個全域變數var mode紀錄當前的模式，例如：mode = 0時為畫筆，並在mousemove的事件裡面判斷當前是哪個mode，並做相對應的事情。

    function解說：
    1. function draw_line(ctx, x1, y1, x2, y2)
        當mode為畫筆時，這個function會在canvas上畫出一條從(x1, y1)到(x2, y2)的線。
        在mousemove的狀態時，將前一次的offsetX跟offsetY帶入(x1, y1)，並將這次的X, Y帶入(x2, y2)，這樣就可以一直畫出連續的線條。
    
    2. function push_image(im)
        這是用來記前後步驟的function。在每一種繪圖結束的時候，我都會截一張圖push進截圖陣列中；並用canvas_step這個變數紀錄當前在陣列中的哪一張圖片。

    3. function undo()
        這是當使用者點下undo按鈕時會進入的function，他會將canvas_step--，並讓canvas回到截圖陣列中的上一張圖。

    4. function redo()
        這是當使用者點下redo按鈕時會進入的function，他會將canvas_step++，並讓canvas回到截圖陣列中的下一張圖。

    5. function change_mode(m)
        每當使用者按下不同功能的按鈕時，都會跳進這個function，依照對應的傳入數字m，改變mode這個變數。

    6. function clear_canvas()
        這個function會用clearRect清出一個canvas大小的空間，讓整個canvas淨空。

    7. function add_text(x, y) \ function handle_enter(e) \ function draw_text(txt)
        這三個function都是用來實作文字輸入框，使用者用滑鼠在canvas上click的時候，如果mode是text，那麼就會進入add_text，在相對應的位置上出現一個文字輸入框（input text）。
        接著若onkeydown，那麼就會進入handle_enter這個function，如果key是enter就會進入draw_text，真的把輸入文字draw到canvas上面，並將文字輸入框刪除。

    8. function upload_image(e)
        這是用來讓使用者上傳圖片的。會開啟一個file reader，並將使用者選擇的圖片draw到canvas上。

    其他實作：
    1. 滑鼠圖片改變：
        滑鼠在change mode的時候會順便改變圖片。

    2. 橡皮擦：
        將canvas的globalCompositeOperation屬性設為destination-out，並在mousemove的時候用跟畫筆一樣的function，這樣新舊圖形交疊的部分就會變成透明的（被擦掉）。
    
    3. 圓形、方形、三角形繪製：
        用兩個canvas交疊，其中一個專門繪製特殊圖形。在mousemove時先把canvas清空再畫下一個位置的圖形，這樣就可以看起來是順著滑鼠拉圖形出來。

    
    

### Gitlab page link
[link](https://108000210.gitlab.io/AS_01_WebCanvas)

     https://108000210.gitlab.io/AS_01_WebCanvas

### Others (Optional)

    Anythinh you want to say to TAs.


<style>
table th{
    width: 100%;
}
</style>