var canvas = document.getElementById('canvas');
var overlay_canvas = document.getElementById('overlay');
var ctx = canvas.getContext("2d");
var overlay_ctx = overlay_canvas.getContext('2d');
var color_picker = document.getElementById('color');
var slider = document.getElementById('slider');
var download = document.getElementById('download');
var font = document.getElementById('font_list');
var font_size = document.getElementById('font_size');
var cursor_arr = ["img/brush.png", "img/erasor.png", "img/text.png", "img/rect.png", "img/round.png", "img/triangle.png"]


var canvas_step = -1;
var is_drawing = false; //drawing or not
var image_arr = new Array(); // step
var mode = 0; 
//mode 0 = brush, 1 = eraser, 2 = text, 
//3 = fill rect, 4 = round, 5 = triangle.
let down_x = 0; //mousedown x
let down_y = 0; //mousedown y
let pre_x = 0;
let pre_y = 0;
let txt_x = 0;
let txt_y = 0;
let x = 0; //mouse x when drawing
let y = 0; //mouse y when drawing
var has_input = false; //text input

overlay_canvas.style.cursor = "url('img/brush.png') 10 10, auto";
//overlay_canvas.style.cursor = "url(./'cursor.png')";

function push_image(im){
    canvas_step++;
    image_arr[canvas_step] = im;
}

function draw_line(ctx, x1, y1, x2, y2) {
    console.log('drawing!');
    var width = slider.value;
    ctx.beginPath();
    ctx.strokeStyle = color_picker.value;
    ctx.lineWidth = width;
    ctx.lineCap = "round";
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.stroke();
    ctx.closePath();
}

function undo(){
    if(canvas_step - 1 >= 0){
        ctx.putImageData(image_arr[--canvas_step], 0, 0);
    }else console.log('undo fail!');
}
function redo(){
    if(canvas_step + 1 <= image_arr.length - 1){
        ctx.putImageData(image_arr[++canvas_step], 0, 0);
    }else console.log('redo fail!');
}

function change_mode(m){
    mode = m;
    overlay_canvas.style.cursor = "url('"+cursor_arr[m]+"') 10 10, auto";

}

function clear_canvas(){
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    push_image(ctx.getImageData(0, 0, canvas.width, canvas.height));
}

//all three are text handler
//add input box to canvas
function add_text(x, y) {

    var t = document.createElement('input');
    t.type = 'text';
    t.style.position = 'fixed';
    t.style.left = x + 'px';
    t.style.top = y + 'px';
    t.onkeydown = handle_enter;

    document.body.appendChild(t);
    t.focus();
    has_input = true;
}


function handle_enter(e) {
    var keyCode = e.keyCode;
    if (keyCode === 13) {
        draw_text(this.value);
        document.body.removeChild(this);
        has_input = false;
    }
}


function draw_text(txt) {
    ctx.textBaseline = 'top';
    ctx.textAlign = 'left';
    ctx.font = font_size.value+"px "+font.value;
    ctx.fillStyle = color_picker.value;
    ctx.fillText(txt, txt_x, txt_y);
    push_image(ctx.getImageData(0, 0, canvas.width, canvas.height));
}

function upload_image(e){
    var reader = new FileReader();
    reader.onload = function(event){
        var img = new Image();
        img.onload = function(){
            ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
            push_image(ctx.getImageData(0, 0, canvas.width, canvas.height));
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);     
}



push_image(ctx.getImageData(0, 0, canvas.width, canvas.height));


download.addEventListener('click', e => {
    download.href = canvas.toDataURL('image/png');
});

overlay_canvas.addEventListener('mousedown', e => {
    down_x = e.offsetX;
    down_y = e.offsetY;
    pre_x = down_x;
    pre_y = down_y;
    x = down_x;
    y = down_y;
    is_drawing = true;
});
overlay_canvas.addEventListener('mousemove', e => {
    if(is_drawing === true){
        if(mode == 0){
            draw_line(ctx, x, y, e.offsetX, e.offsetY);
            x = e.offsetX;
            y = e.offsetY;
        }else if(mode == 1){
            ctx.globalCompositeOperation="destination-out";
            draw_line(ctx, x, y, e.offsetX, e.offsetY);
            x = e.offsetX;
            y = e.offsetY;
            //var e_size = slider.value*3;
            //var e_size = slider.value;
            //ctx.beginPath();
            //ctx.arc(x,y,e_size,0,Math.PI*2);
            //ctx.fill();
            //ctx.clearRect(x-e_size/2, y-e_size/2, e_size, e_size);
            //console.log('erasing!');
            //x = e.offsetX;
            //y = e.offsetY;
        }else if(mode == 3){
            console.log('filling rect!');
            overlay_ctx.clearRect(0, 0, overlay_canvas.width, overlay_canvas.height);
            overlay_ctx.fillStyle = color_picker.value;
            overlay_ctx.fillRect(down_x, down_y, x-down_x, y-down_y);
            pre_x = x;
            pre_y = y;
            x = e.offsetX;
            y = e.offsetY;
        }else if(mode == 4){
            console.log('drawing circle!');
            var x_pow = Math.pow(x-down_x, 2);
            var y_pow = Math.pow(y-down_y, 2);
            var sq = Math.floor(Math.sqrt(x_pow+y_pow));
            overlay_ctx.clearRect(0, 0, overlay_canvas.width, overlay_canvas.height);
            overlay_ctx.strokeStyle = color_picker.value;
            overlay_ctx.lineWidth = slider.value;
            overlay_ctx.beginPath();
            overlay_ctx.arc(down_x, down_y, sq, 0, 2 * Math.PI);
            overlay_ctx.stroke();
            x = e.offsetX;
            y = e.offsetY;
        }else if(mode == 5){
            overlay_ctx.clearRect(0, 0, overlay_canvas.width, overlay_canvas.height);
            overlay_ctx.strokeStyle = color_picker.value;
            overlay_ctx.lineWidth = slider.value;
            overlay_ctx.beginPath();
            overlay_ctx.moveTo(down_x, down_y);
            overlay_ctx.lineTo(x, y);
            overlay_ctx.lineTo(2*down_x-x, y);
            overlay_ctx.closePath();
            overlay_ctx.stroke();
            x = e.offsetX;
            y = e.offsetY;
        }
    }
});
window.addEventListener('mouseup', e => {
    if(is_drawing === true){
        if(mode == 0){
            is_drawing = false;
            push_image(ctx.getImageData(0, 0, canvas.width, canvas.height));
        }else if(mode == 1){
            is_drawing = false;
            ctx.globalCompositeOperation="source-over";
            push_image(ctx.getImageData(0, 0, canvas.width, canvas.height));
        }else if(mode == 2){
            is_drawing = false;
        }else if(mode == 3){
            overlay_ctx.clearRect(0, 0, overlay_canvas.width, overlay_canvas.height);
            ctx.fillStyle = color_picker.value;
            ctx.fillRect(down_x, down_y, pre_x-down_x, pre_y-down_y);
            is_drawing = false;
            push_image(ctx.getImageData(0, 0, canvas.width, canvas.height));
        }else if(mode == 4){
            var x_pow = Math.pow(x-down_x, 2);
            var y_pow = Math.pow(y-down_y, 2);
            var sq = Math.floor(Math.sqrt(x_pow+y_pow));
            overlay_ctx.clearRect(0, 0, overlay_canvas.width, overlay_canvas.height);
            ctx.strokeStyle = color_picker.value;
            ctx.lineWidth = slider.value;
            ctx.beginPath();
            ctx.arc(down_x, down_y, sq, 0, 2 * Math.PI);
            ctx.stroke();
            is_drawing = false;
            push_image(ctx.getImageData(0, 0, canvas.width, canvas.height));
        }else if(mode == 5){
            overlay_ctx.clearRect(0, 0, overlay_canvas.width, overlay_canvas.height);
            ctx.strokeStyle = color_picker.value;
            ctx.lineWidth = slider.value;
            ctx.beginPath();
            ctx.moveTo(down_x, down_y);
            ctx.lineTo(x, y);
            ctx.lineTo(2*down_x-x, y);
            ctx.closePath();
            ctx.stroke();
            is_drawing = false;
            push_image(ctx.getImageData(0, 0, canvas.width, canvas.height));
        }
    }
});


overlay_canvas.onclick = e => {
    if(mode == 2){
        if (has_input) return;
        add_text(e.clientX, e.clientY);
        txt_x = e.offsetX;
        txt_y = e.offsetY;
    }
}

